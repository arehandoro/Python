import levels
import character
import management


class Game:
    """Define class Game."""

    def __init__(self):
        """Initialize Game objects."""

        self.choice = ''

    def menu(self):
        """Return menu chosen.

        options:
        1: Start game.
        2: Load game.
        3: Quit."""

        print("***Welcome to the journey to Mount Qaf***")
        self.choice = input(f"1- Press key '1' or type 'start' to start a new game\n"
                            f"2- Press key '2' or type 'load' to load your progress\n"
                            f"3- Press key '3' or type 'quit' to quit the game\n")
        self.submenu()

    def submenu(self):
        """Go to indicated submenu.

        Options:

        1/start game.
        2/load game.
        3/quit."""

        if self.choice.lower() == '1' or self.choice.lower() == 'start':
            print("Starting a new game...")
            profile = input("Enter a user name to save your progress or type '/b' to go back\n")
            if profile == '/b':
                print('Going back to menu...')
                self.menu()
            else:
                avatar = character.Character()
                avatar.difficulty()
                avatar.show()
                inventory = avatar.inventory
                player = avatar.character
                difficulty = avatar.lives()
                if difficulty == 'easy':
                    life = 5
                    level = levels.Level()
                    level.storyline(1, profile, 1, inventory, player, life, difficulty)
                elif difficulty == 'medium':
                    life = 3
                    level = levels.Level()
                    level.storyline(1, profile, 1, inventory, player, life, difficulty)
                elif difficulty == 'hard':
                    life = 1
                    level = levels.Level()
                    level.storyline(1, profile, 1, inventory, player, life, difficulty)

        elif self.choice.lower() == '2' or self.choice.lower() == 'load':
            print("Type your user name from the list:")
            management.users()
            profile = input()
            saves = management.list_dir()
            if profile + '.json' not in saves:
                print("No save data found!")
                self.menu()
            else:
                file = management.load(profile)
                player = file["char_attrs"]
                inventory = file["inventory"]
                difficulty = file["difficulty"]
                life = int(file["lives"])
                stage = int(file["level"])
                level = levels.Level()
                print("Loading your progress...")
                level.storyline(stage, profile, 4, inventory, player, life, difficulty)

        elif self.choice.lower() == '3' or self.choice.lower() == 'quit':
            print("Goodbye")
            exit()
        else:
            print("Unknown input! Please enter a valid one.")
            self.menu()


if __name__ == '__main__':
    Game().menu()
