class Character:
    """Define character class with inventory and a difficulty method"""

    def __init__(self):
        """Initialize character objects."""

        print("Create your character:")
        self.character = {'name': input('Name\n'), 'species': input('Species\n'), 'gender': input('Gender\n')}
        print("Pack your bag for the journey:")
        self.inventory = {'snack': input("Favourite Snack\n"), 'weapon': input('A weapon for the journey\n'),
                          'tool': input('A traversal tool\n')}
        self.modes = ['1', 'easy', '2', 'medium', '3', 'hard']
        self.mode = ''

    def player(self):
        return self.character.values()

    def items(self):
        return self.inventory.values()

    def difficulty(self):
        """Choose difficulty

        options:
        1. Easy
        2. Medium
        3. Hard"""

        print('Choose your difficulty')
        self.mode = input('1- Easy \n2- Medium \n3- Hard\n')
        if self.mode.lower() in self.modes:
            if self.mode == '1':
                return "Easy"
            elif self.mode == '2':
                return "Medium"
            elif self.mode == '3':
                return "Hard"
            else:
                return self.mode.capitalize()
        else:
            print("Unknown input! Please enter a valid one.")
            self.difficulty()

    def lives(self):
        return self.mode.lower()

    def show(self):
        """Print choices"""
        print('')
        print('Good luck on your journey!')
        print(f"Your character: {self.character['name']}, {self.character['species']}, {self.character['gender']}")
        print(f"Your inventory: {self.inventory['snack']}, {self.inventory['weapon']}, {self.inventory['tool']}")
        print(f"Difficulty: {self.mode}")
        print('')
