import termcolor
import os
import json


def remove_life(life, damage):
    termcolor.cprint(f"You died! Lives remaining: {life - damage}", 'red', attrs=['bold'])
    return life - damage


def add_life(life, damage):
    termcolor.cprint(f"You gained one life! Lives remaining: {life + damage}", 'green', attrs=['bold'])
    return life + damage


def save(profile, character, inventory, life, difficulty, stage):
    try:
        os.mkdir('/home/alejandro/Projects/Text Based Adventure Game/Text Based Adventure Game/task/saves')
    except FileExistsError:
        print("Folder already exists.")
    finally:
        os.chdir('/home/alejandro/Projects/Text Based Adventure Game/Text Based Adventure Game/task/saves')
        save_file = {"char_attrs": character, "inventory": inventory, "difficulty": difficulty,
                     "lives": life, "level": stage}
        with open(f'{profile}.json', 'w+') as f:
            f.write(json.dumps(save_file))
        return "You've found a safe spot to rest. Saving your progress..."


def list_dir():
    try:
        os.chdir('saves')
    except FileNotFoundError:
        os.chdir("/home/alejandro/Projects/Text Based Adventure Game/Text Based Adventure Game/task/saves")
    finally:
        return [i for i in os.listdir()]


def users():
    try:
        os.chdir('saves')
    except FileNotFoundError:
        os.chdir("/home/alejandro/Projects/Text Based Adventure Game/Text Based Adventure Game/task/saves")
    finally:
        for file in os.listdir():
            if '.' in file:
                print(file.rstrip('.json'))
            else:
                print(file)


def load(user):
    try:
        os.chdir('saves')
    except FileNotFoundError:
        os.chdir("/home/alejandro/Projects/Text Based Adventure Game/Text Based Adventure Game/task/saves")
    finally:
        with open("/home/alejandro/Projects/Text Based Adventure Game/Text Based Adventure Game/task/saves/"
                  + user + '.json', 'r') as f:
            load_file = f.read()
    return json.loads(load_file)
