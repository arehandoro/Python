import random
import luhn
import sqlite3
# Write your code here


class SimpleBankingSystem:

    def __init__(self):
        self.conn = sqlite3.connect('card.s3db')
        self.cur = self.conn.cursor()
        self.cur.execute('''CREATE TABLE IF NOT EXISTS card(
                            id INTEGER PRIMARY KEY AUTOINCREMENT,
                            number TEXT,
                            pin TEXT,
                            balance INTEGER DEFAULT 0);''')
        self.conn.commit()
        self.selection = ''
        self.iin = 400000
        self.account = ''
        self.pin = 0
        self.card = ''
        self.number = ''
        self.secret = 0
        self.before_checksum = ''
        self.income = 0
        self.transfer = ''
        self.amount = ''

    def select_number(self):
        self.cur.execute(f"SELECT number FROM card WHERE number={self.number}")
        return str(self.cur.fetchone()).strip("(',')")

    def select_pin(self):
        self.cur.execute(f"SELECT pin FROM card WHERE number={self.number}")
        return str(self.cur.fetchone()).strip("(',')")

    def select_balance(self):
        self.cur.execute(f"SELECT balance FROM card WHERE number={self.number}")
        return int(str(self.cur.fetchone()).strip("(',')"))

    def insert_db(self):
        self.cur.execute(f"INSERT INTO card (number, pin) VALUES ({self.card}, {self.pin});")
        self.conn.commit()
        return self.cur.fetchone()

    def update_income(self):
        self.cur.execute(f"UPDATE card set balance=(balance+{self.income}) WHERE number={self.number}")
        self.conn.commit()
        return str(self.cur.fetchone()).strip("(',')")

    def card_exists(self):
        self.cur.execute(f"SELECT number FROM card WHERE number={self.transfer}")
        return self.cur.fetchone()

    def update_transfer(self):
        self.cur.execute(f"UPDATE card set balance=(balance-{self.amount}) WHERE number={self.number}")
        self.conn.commit()
        self.cur.execute(f"UPDATE card set balance=(balance+{self.amount}) WHERE number={self.transfer}")
        self.conn.commit()
        return self.cur.fetchall()

    def main(self):
        self.selection = input("""1. Create an account\n2. Log into account\n0. Exit\n""")
        if self.selection == '1':
            print(self.create())
            self.main()
        elif self.selection == '2':
            if self.login() is True:
                print("You have successfully logged in!\n")
                self.logged_menu()
            else:
                print("Wrong successfully card number or PIN!")
                self.main()
        elif self.selection == '0':
            self.escape()

    def create(self):
        self.account = random.randint(100000000, 999999999)
        self.pin = str(random.randint(1000, 9999))
        self.before_checksum = str(self.iin) + str(self.account)
        self.card = luhn.append(self.before_checksum)
        self.insert_db()
        return "Your card has been created\nYour card number:\n{}\nYour card PIN:\n{}".format(
            int(self.card),
            int(self.pin))

    def login(self):
        self.number = input("Enter your card number:\n")
        self.secret = input("Enter your PIN:\n")
        if self.number is not None and self.number == self.select_number() and self.secret == self.select_pin():
            return True

    def logged_menu(self):
        self.selection = input("""1. Balance\n2. Add income\n3. Do transfer\n4. Close account\n5. Log out\n0. Exit\n""")
        if self.selection == '1':
            print(self.give_balance())
            self.logged_menu()
        elif self.selection == '2':
            self.add_income()
            self.logged_menu()
        elif self.selection == '3':
            self.do_transfer()
            self.logged_menu()
        elif self.selection == '4':
            self.close_account()
            self.logged_menu()
        elif self.selection == '5':
            self.logout()
        else:
            self.escape()

    def give_balance(self):
        return f"Balance: {self.select_balance()}"

    def add_income(self):
        self.income = int(input("Enter income:\n"))
        self.update_income()
        self.conn.commit()
        print("Income was added!")

    def do_transfer(self):
        self.transfer = input("Enter card number:\n")
        if luhn.verify(self.transfer) is False:
            print("Probably you made a mistake in the card number. Please try again!")
            self.logged_menu()
        elif self.card_exists() is None:
            print("Such card does not exist")
            self.logged_menu()
        elif self.transfer == self.select_number():
            print("You can't transfer money to the same account!")
            self.logged_menu()
        else:
            self.amount = int(input("Enter how much money you want to transfer:\n"))
            if self.amount > self.select_balance():
                print("Not enough money!")
                self.logged_menu()
            elif self.amount <= int(self.select_balance()):
                print(self.update_transfer())
                print("Success!")
                self.logged_menu()

    def close_account(self):
        self.cur.execute(f"DELETE FROM card WHERE number={self.card}")
        self.conn.commit()
        print("The account has been closed!")
        self.main()

    def logout(self):
        print("You have successfully logged out!\n")
        self.main()

    def escape(self):
        self.conn.close()
        print("Bye!")
        exit()


simplebankingsystem = SimpleBankingSystem()
simplebankingsystem.main()
