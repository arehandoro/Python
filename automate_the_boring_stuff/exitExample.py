#!/bin/python3

#We need to import the sys module to later call the exit function
import sys
#This is an infinite loop, as it is always True at the beginning of it.
while True:
    print('Type exit to exit.')
    response = input()
    if response == 'exit':
        sys.exit()
    print('You typed ' + response + '.')