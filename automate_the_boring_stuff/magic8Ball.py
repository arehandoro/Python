# Import random module
import random
# Define getAnswer() function.
def getAnswer(answerNumber):
    if answerNumber == 1:
        return 'It is certain'
    elif answerNumber == 2:
        return 'It is decidedly so'
    elif answerNumber == 3:
        return 'Yes'
    elif answerNumber == 4:
        return 'Reply hazy try again'
    elif answerNumber == 5:
        return 'Ask again later'
    elif answerNumber == 6:
        return 'Concentrate and ask again'
    elif answerNumber == 7:
        return 'My reply is no'
    elif answerNumber == 8:
        return 'Outlook not so good'
    elif answerNumber == 9:
        return 'Very doubtful'
#Evaluate to a random number between 1 and 9 and store it in r.
r = random.randint(1, 9)
# The getAnswer() function is called with r as argument. The value of r is stored in a parameter named answerNumber
# (defined with the function at the top of the script). Depending on the value, a string is returned and stored in a
#variable named fortune.
fortune = getAnswer(r)
# print to the screen.
print(fortune)

# Because return values can be passed as arguments to another function, the last three lines of code can be shortened as
# print(getAnswer(random.randint(1, 9)))