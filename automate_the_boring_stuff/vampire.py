#!/bin/python3

print('What\'s your name?')
name = input()
if name == 'Alice':
    print('Hi, Alice.')
else:
    print('What\'s your age?')
    age = input()
    if int(age) < 12:
        print('You are not Alice, kiddo.')
    elif (int(age) > 100 and int(age) < 2000):
        print('You are not Alice, grannie.')
    elif int(age) > 2000:
        print('Unlike you, Alice is not an undead, immortal vampire.')
    else:
        print('Nice try')

