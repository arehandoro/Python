#Program that rolls dice for games like Dungeons & Dragons.
import random

def oneDfour():
    dado = random.randint(1, 4)
    print('Your score is: ' + str(dado))
#    return dado
def oneDsix():
    dado = random.randint(1, 6)
    print('Your score is: ' + str(dado))
def oneDeight():
    dado = random.randint(1, 8)
    print('Your score is: ' + str(dado))
def oneDten():
    dado = random.randint(1, 10)
    print('Your score is: ' + str(dado))
def oneDtwelve():
    dado = random.randint(1, 12)
    print('Your score is: ' + str(dado))
def oneDtwenty():
    dado = random.randint(1, 20)
    print('Your score is: ' + str(dado))
def oneDhundred():
    dado = random.randint(1, 100)
    print('Your score is: ' + str(dado))
while True:
    print('What die would you like to roll?')
    die = input()
    if (die.startswith('1') == False):
        dei = int(die[0])
        for i in range(dei):
            total = oneDfour()
        print('The total roll makes a score of ' + str(total))
    if (die == 'd4'):
        oneDfour()
    elif (die == 'd6'):
        oneDsix()
    elif (die == 'd8'):
        oneDeight()
    elif (die == 'd10'):
        oneDten()
    elif (die == 'd12'):
        oneDtwelve()
    elif (die == 'd20'):
        oneDtwenty()
    elif (die == 'd100'):
        oneDhundred()
    else:
        print('Please enter the die with format: 1d6, 3d8, 1d20, etc')