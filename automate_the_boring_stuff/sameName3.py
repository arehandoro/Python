def spam():
    global eggs
    eggs = 'spam' # this is the global because there is a global statement.
def bacon():
    eggs = 'bacon' # this is a local
def ham():
    print(eggs) # this is the global because there isn't an assign statement

eggs = 42 # this is the global
spam()
print(eggs)