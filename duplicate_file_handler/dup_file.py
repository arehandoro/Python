#!/usr/bin/env python3

import os
import argparse
import sys
import hashlib


class Duplicate:
    def __init__(self):
        self.cache = {}
        self.hash_cache = {}

    """Get a file type as input and return files of same size with their path and names."""
    def program(self):
        args = sys.argv
        if len(args) == 1:
            print("Directory is not specified")
        else:
            parser = argparse.ArgumentParser()
            parser.add_argument('folder')
            args = parser.parse_args()
            file = input("Enter file format:\n")
            print("Size sorting options:\n1. Descending\n2. Ascending\n")
            self.repeat(args, file)

    def repeat(self, args, file: str):
        order = input("Enter a sorting option:\n")
        if order not in ['1', '2']:
            print("\nWrong option")
            self.repeat(args, file)
        else:
            self.options(args, file, order)

    def options(self, args, file: str, order: str):
        if order == '1':
            order = True
            self.duplicates(args, order, file)
        else:
            order = False
            self.duplicates(args, order, file)

    def duplicates(self, args, reverse: bool, extension: str):
        for root, dirs, files in os.walk(args.folder, topdown=True):
            for f in files:
                file_path = os.path.join(root, f)
                size = os.path.getsize(file_path)
                if f.endswith(extension) or extension == "":
                    if size not in self.cache.keys():
                        self.cache[size] = [file_path]
                    else:
                        self.cache[size].append(file_path)
        self.print_duplicates(reverse)

    def print_duplicates(self, reverse: bool):
        for key, value in sorted(self.cache.items(), reverse=reverse):
            print(f'{key} bytes', *value, sep='\n')
        dupli = input("Check for duplicates?\n")
        if dupli == "yes":
            self.find_hashes(reverse)
        else:
            print("Bye")

    def find_hashes(self, reverse: bool):
        for key in sorted(self.cache.keys(), reverse=reverse):
            for v in self.cache[key]:
                h = hashlib.md5(open(v, "rb").read()).hexdigest()
                if key not in self.hash_cache.keys():
                    new_dict = {key: {h: [f"{v}"]}}
                    self.hash_cache.update(new_dict)
                else:
                    if h not in self.hash_cache[key].keys():
                        new_dict = {h: [f"{v}"]}
                        self.hash_cache[key].update(new_dict)
                    else:
                        self.hash_cache[key][h].append(f"{v}")
        self.remove_non_duplicates(reverse)

    def remove_non_duplicates(self, reverse: bool):
        for key in sorted(self.hash_cache.keys(), reverse=reverse):
            for value in sorted(self.hash_cache[key].keys(), reverse=reverse):
                if len(self.hash_cache[key][value]) < 2:
                    del self.hash_cache[key][value]
        self.print_numerated(reverse)

    def print_numerated(self, reverse: bool):
        n = 1
        dup_files = {}
        for key in sorted(self.hash_cache.keys(), reverse=reverse):
            print(f'{key} bytes')
            for h in sorted(self.hash_cache[key].keys(), reverse=reverse):
                print(f"Hash: {h}")
                for value in sorted(self.hash_cache[key][h], reverse=reverse):
                    print(f"{n}. {value}")
                    dup_files.update({n: value})
                    n += 1
        del_files = input("Delete files?\n")
        if del_files == "yes":
            self.delete_files(dup_files)
        else:
            print("Bye")

    def delete_files(self, dup_files):
        files_to_delete = input("Enter file numbers to delete:\n")
        if len(files_to_delete) == 0 or files_to_delete.replace(" ", "").isdigit() is not True:
            print("Wrong format")
            self.delete_files(dup_files)
        size = 0
        for ftd in files_to_delete.split():
            if int(ftd) not in dup_files.keys():
                print("Wrong format")
                self.delete_files(dup_files)
            else:
                deletion = dup_files[int(ftd)]
                size += os.path.getsize(deletion)
                os.remove(deletion)
        print(f"Total freed up space: {size} bytes")


if __name__ == "__main__":
    Duplicate().program()

