class Markdown:
    def __init__(self):
        self.formatters = ["plain", "bold", "italic", "header", "link", "inline-code",
                           "ordered-list", "unordered-list", "new-line"]
        self.specials = ["!help", "!done"]
        self.option = ''
        self.input = ''
        self.document = ''
        self.label = ''
        self.url = ''
        self.level = 0
        self.rows = 0

    def main(self):
        self.option = input("Choose a formatter:")
        if self.option == "!help":
            print("Available formatters: plain bold italic header link inline-code new-line"
                  "ordered-list unordered-list")
            print("Special commands: !help !done")
            self.main()
        elif self.option == "!done":
            self.exit()
        elif self.option in self.formatters:
            if self.option == "plain":
                self.plain()
            elif self.option == "bold":
                self.bold()
            elif self.option == "italic":
                self.italic()
            elif self.option == "header":
                self.header()
            elif self.option == "link":
                self.link()
            elif self.option == "inline-code":
                self.inline_code()
            elif self.option == "new-line":
                self.new_line()
            elif self.option == "ordered-list" or self.option == "unordered-list":
                self.lists()
        else:
            print("Unknown formatting type or command")
            self.main()

    def text(self):
        self.input = input("Text:")
        return self.input

    def plain(self):
        self.document += self.text()
        print(self.document)
        self.main()

    def bold(self):
        self.document += f"**{self.text()}**"
        print(self.document)
        self.main()

    def italic(self):
        self.document += f"*{self.text()}*"
        print(self.document)
        self.main()

    def inline_code(self):
        self.document += r"`{}`".format(self.text())
        print(self.document)
        self.main()

    def link(self):
        self.label = input("Label:")
        self.url = input("URL:")
        self.document += f"[{self.label}]({self.url})"
        print(self.document)
        self.main()

    def header(self):
        self.level = int(input("Level:"))
        if 1 < self.level > 6:
            print("The level should be within the range of 1 to 6")
            self.header()
        self.document += f"{'#' * self.level} {self.text()}\n"
        print(self.document)
        self.main()

    def new_line(self):
        self.document += "\n"
        print(self.document)
        self.main()

    def lists(self):
        def to_print(x, element):
            return f"{x}. {element}\n"

        self.rows = int(input("Number of rows: "))
        if self.rows < 1:
            print("The number of rows should be greater than zero")
            self.lists()
        if self.option == 'ordered-list':
            for number in range(self.rows):
                el = input(f"Row #{number + 1}: ")
                self.document += to_print(number + 1, el)
            print(self.document)
        else:
            for number in range(self.rows):
                el = input(f"Row #{number + 1}")
                self.document += to_print('*', el)
            print(self.document)
        self.main()

    def exit(self):
        with open('output.md', 'w', encoding='utf-8') as file:
            file.write(self.document)
        exit()


if __name__ == "__main__":
    markdown = Markdown
    markdown().main()
