class MatrixProc:
    def __init__(self):
        self.result = "The result is:"
        self.matrix_calc = []

    def menu(self):
        options = input(f"1. Add matrices\n2. Multiply matrix by a constant\n3. Multiply matrices\n4. Transpose matrix"
                        f"\n5. Calculate a determinant\n6. Inverse matrix\n0. Exit\n")
        if options == '1':
            self.add_matrix(options)
        elif options == '2':
            self.add_matrix(options)
        elif options == '3':
            self.add_matrix(options)
        elif options == '4':
            choice = input(f"1. Main diagonal\n2. Side diagonal\n3. Vertical line\n4. Horizontal line\n"
                           f"Your choice: ")
            self.add_matrix(options, choice)
        elif options == '5':
            self.add_matrix(options)
        elif options == '6':
            self.add_matrix(options)
        elif options == '0':
            exit()

    def add_matrix(self, options, choice=None):
        if options == '2' or options == '4' or options == '5' or options == '6':
            a_rows, a_columns = input("Enter size of matrix: ").split()
            print("Enter matrix:")
            matrix_a = [input().split() for _ in range(int(a_rows))]
            if options == '2':
                number = float(input("Enter constant: "))
                self.multiply_matrix(a_rows, a_columns, matrix_a, number)
            elif options == '4':
                self.transpose(a_rows, a_columns, matrix_a, choice)
            elif options == '5':
                total = self.determinant(matrix_a)
                print(f"The result is: \n{total}\n")
                self.menu()
            elif options == '6':
                total = self.get_matrix_inverse(matrix_a)
                print(f"The result is: \n")
                for x in range(int(a_rows)):
                    print(*total[x])
                print("")
                self.menu()
        elif options == '1' or options == '3':
            a_rows, a_columns = input("Enter size of first matrix: ").split()
            print("Enter first matrix:")
            matrix_a = [input().split() for _ in range(int(a_rows))]

            b_rows, b_columns = input("Enter size of first matrix: ").split()
            print("Enter second matrix:")
            matrix_b = [input().split() for _ in range(int(b_rows))]
            if options == '1':
                self.sum_matrix(a_rows, a_columns, matrix_a, b_rows, b_columns, matrix_b)
            elif options == '3':
                self.matrix_by_matrix(a_rows, a_columns, matrix_a, b_rows, b_columns, matrix_b)

    def create_matrix(self, rows, columns):
        # Reset matrix_calc before creating again to prevent addition
        self.matrix_calc = []
        while len(self.matrix_calc) < int(rows):
            self.matrix_calc.append([])
            while len(self.matrix_calc[-1]) < int(columns):
                self.matrix_calc[-1].append(0.0)
        return self.matrix_calc

    def sum_matrix(self, a_rows, a_columns, matrix_a, b_rows, b_columns, matrix_b):
        self.create_matrix(a_rows, b_columns)
        if a_rows == b_rows and a_columns == b_columns:
            for x in range(int(a_rows)):
                for y in range(int(b_columns)):
                    self.matrix_calc[x][y] = float(matrix_a[x][y]) + float(matrix_b[x][y])
            print(self.result)
            self.representation(a_rows)
        else:
            print("The operation cannot be performed.")
            self.menu()

    def multiply_matrix(self, a_rows, a_columns, matrix_a, number):
        self.create_matrix(a_rows, a_columns)
        for x in range(int(a_rows)):
            for y in range(int(a_columns)):
                self.matrix_calc[x][y] = (number * float(matrix_a[x][y]))
        print(self.result)
        self.representation(a_rows)

    def matrix_by_matrix(self, a_rows, a_columns, matrix_a, b_rows, b_columns, matrix_b):
        # Get matrix of size a_columns and b_rows
        self.create_matrix(a_rows, b_columns)
        if a_columns != b_rows:
            raise ArithmeticError(
                'Number of A columns must equal number of B rows.')
        for x in range(int(a_rows)):
            for y in range(int(b_columns)):
                total = 0
                for z in range(int(a_columns)):
                    total += float(matrix_a[x][z]) * float(matrix_b[z][y])
                self.matrix_calc[x][y] += total
        self.representation(a_rows)

    def transpose(self, a_rows, a_columns, matrix_a, choice):
        if choice == '1':
            self.main_diagonal(a_rows, a_columns, matrix_a)
        elif choice == '2':
            self.side_diagonal(a_rows, a_columns, matrix_a)
        elif choice == '3':
            self.vertical_line(a_rows, a_columns, matrix_a)
        elif choice == '4':
            self.horizontal_line(a_rows, a_columns, matrix_a)

    def main_diagonal(self, a_rows, a_columns, matrix_a):
        self.create_matrix(a_rows, a_columns)
        # From left to right and from top to bottom, then rows and columns order swapped in new matrix.
        for x in range(int(a_rows)):
            for y in range(int(a_columns)):
                self.matrix_calc[x][y] = matrix_a[y][x]
        self.representation(a_rows)

    def side_diagonal(self, a_rows, a_columns, matrix_a):
        self.create_matrix(a_rows, a_columns)
        # From left to right and from top to bottom,
        # Then rest rows/columns minus 1 to have length matrix, and then rest iteration.
        for x in range(int(a_rows)):
            for y in range(int(a_columns)):
                self.matrix_calc[x][y] = matrix_a[(int(a_rows) - 1) - y][(int(a_columns) - 1) - x]
        self.representation(a_rows)

    def vertical_line(self, a_rows, a_columns, matrix_a):
        self.create_matrix(a_rows, a_columns)
        # From left to right and top to bottom. Rest columns to change order of matrix.
        for x in range(int(a_rows)):
            for y in range(int(a_columns)):
                self.matrix_calc[x][y] = matrix_a[x][(int(a_columns) - 1) - y]
        self.representation(a_rows)

    def horizontal_line(self, a_rows, a_columns, matrix_a):
        self.create_matrix(a_rows, a_columns)
        # From left to right and top to bottom. Rest rows to change order of matrix.
        for x in range(int(a_rows)):
            for y in range(int(a_columns)):
                self.matrix_calc[(int(a_rows) - 1) - x][y] = matrix_a[x][y]
        self.representation(a_rows)

    def minor(self, matrix_a, i, j):
        if len(matrix_a[0]) == 2:
            return float(matrix_a[0][0]) * float(matrix_a[1][1]) - float(matrix_a[0][1]) * float(matrix_a[1][0])
        elif len(matrix_a[0]) > 2:
            ml = [[float(matrix_a[v][w]) for w in range(len(matrix_a[v])) if w != j]
                  for v in range(len(matrix_a)) if v != i]
            return ml

    def determinant(self, matrix_a):
        if type(matrix_a) is not list:
            return matrix_a
        elif len(matrix_a) == 1:
            return matrix_a[0][0]
        elif len(matrix_a) == 2:
            return (-1)**2 * self.determinant(self.minor(matrix_a, 0, 0))
        elif len(matrix_a) > 2:
            determinant_recursion = 0.0
            for i in range(len(matrix_a[0])):
                determinant_recursion += float(matrix_a[0][i]) * ((-1) ** (i + 2)) \
                                         * self.determinant(self.minor(matrix_a, 0, i))
            return determinant_recursion

    def get_matrix_inverse(self, matrix_a):
        det = self.determinant(matrix_a)
        # special case for 2x2 matrix:
        if len(matrix_a) == 2:
            return [[matrix_a[1][1] / det, -1 * matrix_a[0][1] / det],
                    [-1 * matrix_a[1][0] / det, matrix_a[0][0] / det]]
        # find matrix of cofactors
        cofactors = []
        for r in range(len(matrix_a)):
            cofactor_row = []
            for c in range(len(matrix_a)):
                minor = self.minor(matrix_a, r, c)
                cofactor_row.append(((-1) ** (r + c)) * self.determinant(minor))
            cofactors.append(cofactor_row)
        cofactors = self.transpose_matrix(cofactors)
        for r in range(len(cofactors)):
            for c in range(len(cofactors)):
                cofactors[r][c] = float(cofactors[r][c]) / det
        return cofactors

    def transpose_matrix(self, cofactors):
        return list(map(list, zip(*cofactors)))

    def representation(self, a_rows):
        # Each row is a list, iterate and expand to print them in separate lines
        for x in range(int(a_rows)):
            print(*self.matrix_calc[x])
        print("")
        self.menu()


num_matrix_proc = MatrixProc()
num_matrix_proc.menu()
