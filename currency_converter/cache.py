def initiate_cache(coin, rates, cache):
    if coin != 'usd' and coin != 'eur':
        cache.update({'usd': rates['usd']['rate']})
        cache.update({'eur': rates['eur']['rate']})
    elif coin == 'usd' or coin == 'eur':
        write_cache(coin, rates, cache)
    return cache


def write_cache(coin, rates, cache):
    if coin == 'usd':
        cache.update({coin: rates[coin]['rate']})
        cache.update({'eur': rates['eur']['rate']})
    else:
        cache.update({coin: rates[coin]['rate']})
    return cache


def read_cache(coin, cache):
    # print(cache)
    return cache[coin]
