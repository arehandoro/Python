import requests


def exchange(currency):
    # Save the results directly as json
    return requests.get('http://floatrates.com/daily/' + currency.lower() + '.json').json()
