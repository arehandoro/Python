import cache
import get_exchange


class CurrencyConverter:
    def __init__(self):
        self.coin = ''
        self.cache = {}

    def main(self):
        self.coin = input().upper()
        r = get_exchange.exchange(self.coin)
        self.info(r)

    def info(self, r):
        exchange = input().upper()
        if not exchange:
            exit()
        if self.cache == {}:
            self.cache = cache.initiate_cache(exchange.lower(), r, self.cache)
            money = float(input())
            self.currency(money, exchange, r)
        else:
            money = float(input())
            self.currency(money, exchange, r)

    def currency(self, money, exchange, r):
        print("Checking the cache...")
        if exchange.lower() in self.cache.keys():
            print("Oh! It is in the cache!")
            print("You received {} {}.".format(round(money *
                                                     cache.read_cache(exchange.lower(), self.cache), 2),
                                               exchange.upper()))
        else:
            print("Sorry, but it is not in the cache!")
            cache.write_cache(exchange.lower(), r, self.cache)
            print("You received {} {}.".format(round(money *
                                                     cache.read_cache(exchange.lower(), self.cache), 2),
                                               exchange.upper()))

        self.info(r)


if __name__ == "__main__":
    CurrencyConverter().main()
