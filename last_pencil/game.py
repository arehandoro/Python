import string
import random


class LastPencil:
    def __init__(self):
        self.player = 'John'
        self.bot = 'Jack'
        self.name_selection = string.Template("Who will be the self.turn ($name1, $name2):\n")
        self.name_error = string.Template("Choose between $name1 and $name2")
        self.pencils = 0
        self.taken = 0
        self.numeric_error = "The number of pencils should be numeric"
        self.value_error = "Possible values: '1', '2' or '3'"
        self.turn = ''

    def main(self):
        try:
            self.pencils = int(input("How many pencils would you like to use: "))
        except ValueError:
            print(self.numeric_error)
            self.main()
        else:
            if self.pencils < 1:
                print("The number of pencils should be positive")
                self.main()
        self.player_selection()

    def player_selection(self):
        self.turn = input(self.name_selection.substitute(name1=self.player, name2=self.bot))
        if self.turn == self.player:
            print(f"{self.pencils * '|'}")
            self.player_turn()
        elif self.turn == self.bot:
            print(f"{self.pencils * '|'}")
            self.bot_turn()
        else:
            print(self.name_error.substitute(name1=self.player, name2=self.bot))
            self.player_selection()

    def player_turn(self):
        print(f"{self.player}'s turn!")
        try:
            self.taken = int(input())
        except ValueError:
            print(self.value_error)
            self.player_turn()
        else:
            if self.taken > 3 or self.taken < 1:
                print(self.value_error)
                self.player_turn()
            elif self.taken > self.pencils:
                print("Too many pencils were taken")
                self.player_turn()
        self.winner()

    def bot_turn(self):
        print(f"{self.bot}'s turn:")
        if self.pencils == 1:
            self.taken = 1
            print(self.taken)
            self.winner()
        if (self.pencils - 5) % 4 == 0 or self.pencils == 5:
            self.taken = random.randint(1, 3)
            print(self.taken)
            self.winner()
        if (self.pencils - 4) % 4 == 0 or self.pencils == 4:
            self.taken = 3
            print(self.taken)
            self.winner()
        if (self.pencils - 3) % 4 == 0 or self.pencils == 3:
            self.taken = 2
            print(self.taken)
            self.winner()
        if (self.pencils - 2) % 4 == 0 or self.pencils == 2:
            self.taken = 1
            print(self.taken)
            self.winner()

    def winner(self):
        self.pencils = self.pencils - self.taken
        if self.pencils <= 0 and self.turn == self.bot:
            print(f"{self.player} won!")
            exit()
        if self.pencils <= 0 and self.turn == self.player:
            print(f"{self.bot} won!")
            exit()
        if self.pencils > 0 and self.turn == self.bot:
            self.turn = self.player
            print(self.pencils * '|')
            self.player_turn()
        if self.pencils > 0 and self.turn == self.player:
            self.turn = self.bot
            print(self.pencils * '|')
            self.bot_turn()


if __name__ == '__main__':
    last_pencil = LastPencil()
    last_pencil.main()
