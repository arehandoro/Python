### Python Developer Hyperskill Course

## Simple Programs

# Introduction to Python
-----------------------

Python is a modern general-purpose programming language designed to be easy to learn and use in practice.
Python has a wide range of possible applications, especially in:

* web development
* data science (including machine learning)
* scripting (task automation, e.g. text processing or a simulation of typical user actions)

Less commonly, it is also used in desktop development.

**Example**

Code that prints **Learn Python to be great!**

`print("Learn Python to be great!")`

# Overview of the basic program
------------------------------

**Hello, World!** example

`print("Hello, World!")`

**print** is the name of a function. A **function** is a block of code that does something useful. To include quotes in a string it must be **enclosed** in quotes of another type.

`print("Yes, I'm ready to learn Python.")`

_Possible errors_

* Extra indentation
* Calling function by wrong name
* Writing names in wrong case
* Missing one or both quotes for a string
* Missing one or more parentheses

# Multi-line programs
---------------------

The **print** function allows to print an empty line with no string specified.

# Program execution
-------------------

**Python is an interpreted language.** At a high level, program execution is like this:

`Writing a script` ---> `Interpretation` ---> `Everything works!`

The interpreter is the software that "reads" the program and executes the code line by line from top to bottom.
It is a part of the **standard library** with all built-in modules, functions, data, etc.
The default interpreter for Python is written in C and is called CPython.
PyPy (Restricted Python), Jython (Java) and IronPython (.Net) are other available interpreters.

Interpretation consists of 3 steps; compilation, byte-code and virtual machine.
The compiler turns code into byte-code. **Byte-code** is _lower level_, _platform-independent_ and a _more efficient_ version of the code.
It is not binary machine code, but **Python-specific** representation of source code. This is the reason Python is slower than traditional compiled languages.

After compilation, the byte-code is given to the **PVM (Python Virtual Machine).** which runs the byte-code.

# Introduction to Python shell
------------------------------

The interactive shell is a real-time Python interpreter. IDLE is a simple graphic interface for Python Shell.

# Taking input
--------------

The `input()` function waits for a value to be entered by the user, it reads it and returns it in a program as a string.
Recommended to state clearly what type of input is expected. For example, `input()` can take an optional argument for this: `input('Please, enter your name: ')` (input is printed in new line)
but it can also be printed with the `print()`function (input is printed in the same line)

# Program with numbers
----------------------



