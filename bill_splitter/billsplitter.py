import random


class BillSplitter:

    def __init__(self):
        try:
            self.number = int(input("Enter the number of friends joining (including you):\n"))
        except ValueError:
            print("Please enter a digit")
        else:
            if self.number < 1:
                print("No one is joining for the party")
                exit()
            else:
                print("Enter the name of every friend (including you), each on a new line:")
                self.friends = {input(): 0 for _ in range(self.number)}
                self.total = int(input("Enter the total bill value:\n"))
                self.lucky = input('Do you want to use the "Who is lucky" feature? Write Yes/No:\n')

    def people(self):
        if self.lucky == "Yes":
            lucky_one = random.choice(list(self.friends))
            print(f"{lucky_one} is the lucky one")
            self.friends = {key: (round(self.total / (self.number - 1), 2) if key != lucky_one else 0)
                            for (key, value) in self.friends.items()}
            print(self.friends)
            exit()
        else:
            print("No one is going to be lucky")
            self.friends = {key: round(self.total / self.number, 2) for (key, value) in self.friends.items()}
            print(self.friends)
            exit()


if __name__ == "__main__":
    BillSplitter().people()
