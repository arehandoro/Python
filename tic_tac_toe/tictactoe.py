# write your code here
class TicTacToe:

    def __init__(self):
        self.game = '         '
        self.entry = []
        self.matrix = []
        self.turn = 0

    def main(self):
        self.matrix = [[x for x in self.game[:3]], [x for x in self.game[3:6]], [x for x in self.game[6:]]]
        self.phase()
        self.coordinates()

    def phase(self):
        print('---------')
        print('|', ' '.join(self.matrix[0][:]), '|')
        print('|', ' '.join(self.matrix[1][:]), '|')
        print('|', ' '.join(self.matrix[2][:]), '|')
        print('---------')

    # noinspection PyTypeChecker
    def coordinates(self):
        self.entry = input("Enter the coordinates: ").split()
        self.entry[0] = int(self.entry[0]) - 1
        self.entry[1] = 3 - int(self.entry[1])
        if self.error_coordinates() is not None:
            print(self.error_coordinates())
            self.coordinates()
        else:
            self.move()

    def move(self):
        self.turn += 1
        if self.turn % 2 == 1:
            self.matrix[self.entry[1]][self.entry[0]] = 'X'
            self.state()
        elif self.turn % 2 == 0:
            self.matrix[self.entry[1]][self.entry[0]] = 'O'
            self.state()

    def error_coordinates(self):
        if type(self.entry[0]) is False or type(self.entry[1]) is False:
            return "You should enter numbers!"
        elif 0 < self.entry[0] > 2 or 0 < self.entry[1] > 2:
            return "Coordinates should be from 1 to 3!"
        elif self.matrix[self.entry[1]][self.entry[0]] != ' ':
            return "This cell is occupied! Choose another one!"

    def state(self):
        if self.x_wins() is "X wins" and self.o_wins() is False:
            self.phase()
            print(self.x_wins())
            exit()
        elif self.o_wins() is "O wins" and self.x_wins() is False:
            self.phase()
            print(self.o_wins())
            exit()
        elif self.draw() is not False:
            self.phase()
            print(self.draw())
            exit()
        else:
            self.phase()
            self.coordinates()

    def x_wins(self):
        if self.matrix[0][0] == 'X' and self.matrix[0][1] == 'X' and self.matrix[0][2] == 'X' \
                or self.matrix[1][0] == 'X' and self.matrix[1][1] == 'X' and self.matrix[1][2] == 'X' \
                or self.matrix[2][0] == 'X' and self.matrix[2][1] == 'X' and self.matrix[2][2] == 'X' \
                or self.matrix[0][0] == 'X' and self.matrix[1][0] == 'X' and self.matrix[2][0] == 'X' \
                or self.matrix[0][1] == 'X' and self.matrix[1][1] == 'X' and self.matrix[2][1] == 'X' \
                or self.matrix[0][2] == 'X' and self.matrix[1][2] == 'X' and self.matrix[2][2] == 'X' \
                or self.matrix[0][2] == 'X' and self.matrix[1][1] == 'X' and self.matrix[2][0] == 'X' \
                or self.matrix[0][0] == 'X' and self.matrix[1][1] == 'X' and self.matrix[2][2] == 'X':
            return "X wins"
        else:
            return False

    def o_wins(self):
        if self.matrix[0][0] == 'O' and self.matrix[0][1] == 'O' and self.matrix[0][2] == 'O' \
                or self.matrix[1][0] == 'O' and self.matrix[1][1] == 'O' and self.matrix[1][2] == 'O' \
                or self.matrix[2][0] == 'O' and self.matrix[2][1] == 'O' and self.matrix[2][2] == 'O' \
                or self.matrix[0][0] == 'O' and self.matrix[1][0] == 'O' and self.matrix[2][0] == 'O' \
                or self.matrix[0][1] == 'O' and self.matrix[1][1] == 'O' and self.matrix[2][1] == 'O' \
                or self.matrix[0][2] == 'O' and self.matrix[1][2] == 'O' and self.matrix[2][2] == 'O' \
                or self.matrix[0][2] == 'O' and self.matrix[1][1] == 'O' and self.matrix[2][0] == 'O' \
                or self.matrix[0][0] == 'O' and self.matrix[1][1] == 'O' and self.matrix[2][2] == 'O':
            return "O wins"
        else:
            return False

    def draw(self):
        if not any(1 for spaces in self.matrix if spaces == ' ') and self.turn == 9 \
                and self.x_wins() is False and self.o_wins() is False:
            return "Draw"
        else:
            return False


tictactoe = TicTacToe()
tictactoe.main()
