import random


class RockPaperScissors:

    def __init__(self):
        self.game_options = []
        self.copy_options = []
        self.file = ''
        self.player = ''
        self.default = ['scissors', 'rock', 'paper']
        self.computer = ''
        self.default_lose_options = {'scissors': 'paper', 'paper': 'rock', 'rock': 'scissors'}
        self.greetings = ''
        self.score = 0

    def main(self):
        self.greetings = input("Enter your name:")
        print(f"Hello, {self.greetings}")
        print("Okay, let's start")
        self.game_options = input().split(sep=',')
        self.menu()

    def menu(self):
        if len(self.game_options) == 1:
            self.player = input()
            self.computer = random.choice(self.default)
            self.game()
        else:
            self.player = input()
            self.default = self.game_options[:]
            self.computer = random.choice(self.default)
            if self.player in self.default:
                self.default_lose_options = {
                    'water' : ['scissors', 'fire', 'rock', 'hun', 'lightning', 'devil', 'dragon'],
                    'dragon' : ['snake', 'scissors', 'fire', 'rock', 'gun', 'lightning', 'devil'],
                    'devil' : ['tree', 'human', 'snake', 'scissors', 'fire', 'rock', 'gun'],
                    'gun' : ['wolf', 'tree', 'human', 'snake', 'scissors', 'fire', 'rock'],
                    'rock' : ['sponge', 'wolf', 'tree', 'human', 'snake', 'scissors', 'fire'],
                    'fire' : ['paper', 'sponge', 'wolf', 'tree', 'human', 'snake', 'scissors'],
                    'scissors' : ['air', 'paper', 'sponge', 'wolf', 'tree', 'human', 'snake'],
                    'snake' : ['water', 'air', 'paper', 'sponge', 'wolf', 'tree', 'human'],
                    'human' : ['dragon', 'water', 'air', 'paper', 'sponge', 'wolf', 'tree'],
                    'tree' : ['devil', 'dragon', 'water', 'air', 'paper', 'sponge', 'wolf'],
                    'wolf' : ['lightning', 'devil', 'dragon', 'water', 'air', 'paper', 'sponge'],
                    'sponge' : ['gun', 'lightning', 'devil', 'dragon', 'water', 'air', 'paper'],
                    'paper' : ['rock', 'gun', 'lightning', 'devil', 'dragon', 'water', 'air'],
                    'air' : ['fire', 'rock', 'gun', 'lightning', 'devil', 'dragon', 'water'],
                    'lightning' : ['tree', 'human', 'snake', 'scissors', 'fire', 'rock', 'gun']}
                self.game()
                """if self.default.index(self.player) + len(self.default) // 2 <= len(self.default):
                    self.default_lose_options = self.default[self.default.index(self.player) + 1:
                                                             (self.default.index(self.player) + 1)
                                                             + len(self.default) // 2]
                    self.default_lose_options = {self.player: self.default_lose_options}
                    self.game()
                else:
                    to_right = len(self.default) - (self.default.index(self.player) + 1)
                    from_left = (len(self.default) // 2) - to_right
                    self.default_lose_options = self.default[self.default.index(self.player) + 1:
                                                             len(self.default_lose_options)] + self.default[:from_left]
                    self.default_lose_options = {self.player: self.default_lose_options}
                    self.game()"""
            else:
                self.game()

    def game(self):
        if self.player == '!exit':
            print("Bye!")
            exit()
        elif self.player == '!rating':
            print(self.rating())
            self.menu()
        elif self.player not in self.default:
            print("Invalid input")
            self.menu()
        else:
            if self.player == self.computer:
                print(self.draw())
                self.menu()
            elif self.computer in self.default_lose_options[self.player]:
                print(self.win())
                self.menu()
            elif self.computer not in self.default_lose_options[self.player]:
                print(self.lose())
                self.menu()

    def rating(self):
        self.file = open('rating.txt', 'r')
        for lines in self.file:
            if self.greetings == lines.split()[0]:
                self.score += int(lines.split()[1])
                self.file.close()
                return self.score
        else:
            return self.score

    def win(self):
        self.score += 100
        return f"Well done. The computer chose {self.computer} and failed"

    def draw(self):
        self.score += 50
        return f"There is a draw ({self.computer})"

    def lose(self):
        return f"Sorry, but the computer chose {self.computer}"


rps = RockPaperScissors()
rps.main()
